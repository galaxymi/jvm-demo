import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.2.41"
    kotlin("plugin.allopen") version "1.2.41"
    kotlin("plugin.spring") version "1.2.41"
    id("org.springframework.boot") version "2.0.1.RELEASE"
    id("io.spring.dependency-management") version "1.0.5.RELEASE"
}

var kotlin_version: String by extra
kotlin_version = "1.2.41"

dependencies {
    compile(kotlin("stdlib-jdk8", kotlin_version))
    compile(kotlin("reflect", kotlin_version))
    compile("org.springframework.boot","spring-boot-starter-web")
    compile("com.fasterxml.jackson.module", "jackson-module-kotlin")
    testCompile("org.springframework.boot", "spring-boot-starter-test")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}
