import org.jetbrains.kotlin.gradle.dsl.Coroutines
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    application
    kotlin("jvm") version "1.2.41"
}

application {
    mainClassName = "learn.AppKt"
}

dependencies {
    compile(kotlin("stdlib-jdk8", "1.2.41"))
    compile("io.ktor", "ktor-server-netty", "0.9.2")
    compile("ch.qos.logback", "logback-classic", "1.2.1")
    testCompile("junit", "junit", "4.12")
}

kotlin {
    experimental.coroutines = Coroutines.ENABLE
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}
