package learn

import org.scalatest.FunSuite

class OrderTest extends FunSuite {
  test("total order") {
    val order = new Order("Enchilada", 1.5, 2)
    assert(order.total == 3)
    assert(order.receipt == "You ordered 2 Enchilada. Please pay $3.0.")
  }
}
