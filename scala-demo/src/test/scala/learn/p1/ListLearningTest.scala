package learn.p1

import org.scalatest.FunSuite

class ListLearningTest extends FunSuite {
  test("List") {
    val l = List(1, 2, 3)
    assert(l == 1 :: 2 :: 3 :: List())
    assert(l.take(2) == List(1, 2))
    assert(l.takeWhile(_%2 == 0) == List())
    assert(l.forall(_ < 5))
    assert(l.exists(_ > 2))
  }
}
