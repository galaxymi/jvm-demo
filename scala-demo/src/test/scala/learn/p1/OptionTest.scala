package learn.p1

import org.scalatest.FunSuite

class OptionTest extends FunSuite {
  test("Option") {
    val s = Some(1)
    val n: Option[Int] = None
    assert(s.map(_+1) == Some(2))
    assert(n.map(x => x) == None)

    assert(s.getOrElse(0) == 1)
    assert(n.getOrElse(0) == 0)

    assert(s.flatMap(n => Some(n + 1)) == Some(2))
    assert(n.flatMap(n => Some(n + 1)) == None)

    assert(s.orElse(Some(2)) == Some(1))
    assert(n.orElse(Some(2)) == Some(2))

    assert(s.filter(_ == 1) == Some(1))
    assert(s.filter(_ == 2) == None)
    assert(n.filter(_ == 1) == None)
  }

  test("mean") {
    assert(Option.mean(List()) == None)
    assert(Option.mean(List(1.0, 2.0, 3.0, 4.0)) == Some(2.5))
  }

  test("map2") {
    def add(a: Int, b: Int): Int = a + b
    def a = Some(1)
    def b = Some(2)
    assert(Option.map2(a, b)(add) == Some(3))
    assert(Option.map2(a, None)(add) == None)
    assert(Option.map2(None, None)(add) == None)
  }

  test("sequence") {
    assert(Option.sequence(List(Some(1))) == Some(List(1)))
    assert(Option.sequence(List(Some(1), Some(2))) == Some(List(1, 2)))
    assert(Option.sequence(List(None, Some(2))) == None)
    assert(Option.sequence(List(Some(1), None, Some(2))) == None)
  }

  test("traverse") {
    def transform(a: Int): Option[Int] =
      if (a == 2) None else Some(a*a)

    assert(Option.traverse(List(1))(transform) == Some(List(1)))
    assert(Option.traverse(List(1, 3))(transform) == Some(List(1, 9)))
    assert(Option.traverse(List(1, 2))(transform) == None)
    assert(Option.traverse(List(2, 1))(transform) == None)
  }
}
