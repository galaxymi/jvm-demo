package learn.p1

import org.scalatest.FunSuite

class Chapter2Test extends FunSuite {
  test("Chapter2.fib") {
    assert(Chapter2.fib(1) == 0)
    assert(Chapter2.fib(2) == 1)
    assert(Chapter2.fib(3) == 1)
    assert(Chapter2.fib(4) == 2)
    assert(Chapter2.fib(5) == 3)
    assert(Chapter2.fib(6) == 5)
  }

  test("Chapter2.isSorted") {
    assert(Chapter2.isSorted(Array(), (a: Int, b: Int) => a < b))
    assert(Chapter2.isSorted(Array(1), (a: Int, b: Int) => a < b))
    assert(Chapter2.isSorted(Array(1, 2, 3), (a: Int, b: Int) => a < b))
    assert(!Chapter2.isSorted(Array(1, 3, 2), (a: Int, b: Int) => a < b))
    assert(!Chapter2.isSorted(Array(1, 3, 3), (a: Int, b: Int) => a < b))
    assert(Chapter2.isSorted(Array(1, 3, 3), (a: Int, b: Int) => a <= b))
  }

  test("Chapter2.curry") {
    def add(a: Int, b: Int): Int = a + b
    val curryedAdd = Chapter2.curry(add)
    val uncurryedAdd = Chapter2.uncurry(curryedAdd)
    assert(curryedAdd(1)(2) == 3)
    assert(uncurryedAdd(1,2) == 3)
  }

  test("Chapter2.compose") {
    def inc(a: Int): Int = a+1
    def dec(a: Int): Int = a-1
    assert(Chapter2.compose(inc, dec)(2) == 2)
  }
}
