package learn.p1

import org.scalatest.FunSuite

class Chapter3Test extends FunSuite {
  test("List.sum") {
    assert(QList.sum(QList(1, 2, 3)) == 6)
  }

  test("List.product") {
    assert(QList.product(QList()) == 1)
    assert(QList.product(QList(1.5, 2, 3)) == 9)
    assert(QList.product(QList(1.5)) == 1.5)
  }

  test("List.drop") {
    assert(QList.drop(QList(1, 3, 4, 5), 2) == QList(4, 5))
  }

  test("List.dropWhile") {
    assert(QList.dropWhile(QList(1, 2, 3, 4, 5, 6))(x => x%2 == 0) == QList(1, 3, 5))
  }

  test("List.init") {
    assert(QList.init(Nil) == Nil)
    assert(QList.init(QList(1)) == Nil)
    assert(QList.init(QList(1, 2)) == QList(1))
    assert(QList.init(QList(1, 2, 3)) == QList(1, 2))
  }

  test("List.length") {
    assert(QList.length(Nil) == 0)
    assert(QList.length(QList(1)) == 1)
    assert(QList.length(QList("a", "b")) == 2)
    assert(QList.length(QList("a", "b", 1)) == 3)
  }

  test("List.reverse") {
    assert(QList.reverse(Nil) == Nil)
    assert(QList.reverse(QList(1)) == QList(1))
    assert(QList.reverse(QList(1, 2)) == QList(2, 1))
    assert(QList.reverse(QList(1, 2, 3)) == QList(3, 2, 1))
  }

  test("List.append") {
    assert(QList.append(Nil, 1) == QList(1))
    assert(QList.append(QList(1), 2) == QList(1, 2))
    assert(QList.append(QList(1, 2), 3) == QList(1, 2, 3))
  }

  test("List.concat") {
    assert(QList.concat(QList()) == QList())
    assert(QList.concat(QList(QList())) == QList())
    assert(QList.concat(QList(QList(2))) == QList(2))
    assert(QList.concat(QList(QList(1, 2), QList(3), QList(4, 5, 6))) == QList(1, 2, 3, 4, 5, 6))
  }

  test("List.map") {
    assert(QList.map(QList(1, 2, 3))(x => x + 1) == QList(2, 3, 4))
    assert(QList.map(QList(1.1, 2.2, 3.3))(x => x.toString) == QList("1.1", "2.2", "3.3"))
  }

  test("List.filter") {
    assert(QList.filter(QList(1, 2, 3, 4, 5))(x => x%2 == 0) == QList(2, 4))
  }

  test("List.flatMap") {
    assert(QList.flatMap(QList(1, 2, 3))(i => QList(i, i)) == QList(1, 1, 2, 2, 3, 3))
  }

  test("List.filterViaFlatMap") {
    assert(QList.filterViaFlatMap(QList(1, 2, 3, 4, 5))(x => x%2 == 0) == QList(2, 4))
  }

  test("List.zipWith") {
    assert(QList.zipWith(QList(1, 2, 3), QList(4, 5, 6))(_ + _) == QList(5, 7, 9))
  }

  test("Tree.size") {
    assert(Tree.size(Leaf(1)) == 1)
    assert(Tree.size(Branch(Leaf(1), Leaf(1), 1)) == 3)
    assert(Tree.size(Branch(Leaf(1), null, 1)) == 2)
  }

  test("Tree.maximum") {
    assert(Tree.maximum(Leaf(1)) == 1)
    assert(Tree.maximum(Branch(Leaf(2), Leaf(3), 1)) == 3)
    assert(Tree.maximum(Branch(Leaf(1), null, 2)) == 2)
  }

  test("Tree.depth") {
    assert(Tree.depth(null) == 0)
    assert(Tree.depth(Leaf(1)) == 1)
    assert(Tree.depth(Branch(Leaf(2), Leaf(3), 1)) == 2)
  }

  test("Tree.map") {
    assert(Tree.map(null: Tree[Int])(_ * -1) == null)
    assert(Tree.map(Leaf(1))(_ * -1) == Leaf(-1))
    assert(Tree.map(Branch(Leaf(1), null, 2))(_ * -1) == Branch(Leaf(-1), null, -2))
  }
}
