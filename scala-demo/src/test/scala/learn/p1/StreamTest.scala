package learn.p1

import org.scalatest.FunSuite
import learn.p1.stream.Stream

class StreamTest extends FunSuite {
  test("headOption") {
    assert(Stream.empty.headOption.isEmpty)
    assert(Stream.apply(1).headOption.contains(1))
    assert(Stream.apply(1, 2).headOption.contains(1))
  }

  test("toList") {
    assert(Stream.empty.toList == scala.Nil)
    assert(Stream.apply(1).toList == List(1))
    assert(Stream.apply(1, 2, 3).toList == List(1, 2, 3))
  }

  test("take") {
    assert(Stream.empty.take(2) == scala.Nil)
    assert(Stream.apply(1).take(2) == List(1))
    assert(Stream.apply(1, 2, 3).take(2) == List(1, 2))
  }
}
