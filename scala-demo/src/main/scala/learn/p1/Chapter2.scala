package learn.p1

import scala.annotation.tailrec

object Chapter2 {
  def fib(n: Int): Int = {
    @tailrec
    def nthFib(n: Int, i: Int, n1: Int, n2: Int): Int = {
      if (n == i) n2
      else nthFib(n, i+1, n2, n1+n2)
    }
    if (n == 1) 0
    else nthFib(n, 2, 0, 1)
  }

  def isSorted[A](as: Array[A], ordered: (A, A) => Boolean): Boolean = {
    def loop(n: Int, r: Boolean): Boolean =
      if (n >= as.length) r
      else loop(n+1, r && ordered(as(n-1), as(n)))
    loop(1, r = true)
  }

  def curry[A,B,C](f: (A, B) => C): A => B => C = (a: A) => (b: B) => f(a, b)

  def uncurry[A,B,C](f: A => B => C): (A, B) => C = (a: A, b: B) => f(a)(b)

  def compose[A,B,C](f: B => C, g: A => B): A => C = (a: A) => f(g(a))

}
