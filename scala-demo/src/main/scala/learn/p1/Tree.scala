package learn.p1

sealed trait Tree[+A]
case class Leaf[A](value: A) extends Tree[A]
case class Branch[A](left: Tree[A], right: Tree[A], value: A) extends Tree[A]

object Tree {
  def size[A](t: Tree[A]): Integer = t match {
    case null => 0
    case Leaf(_) => 1
    case Branch(l, r, _) => size(l) + size(r) + 1
  }

  def maximum(t: Tree[Int]): Int = t match {
    case null => Integer.MIN_VALUE
    case Leaf(v) => v
    case Branch(l, r, v) => v max maximum(l) max maximum(r)
  }

  def depth[A](t: Tree[A]): Int = t match {
    case null => 0
    case Leaf(_) => 1
    case Branch(l, r, _) => 1 + (depth(l) max depth(r))
  }

  def map[A, B](t: Tree[A])(f: A => B): Tree[B] = t match {
    case null => null
    case Leaf(v) => Leaf(f(v))
    case Branch(l, r, v) => Branch(map(l)(f), map(r)(f), f(v))
  }
}