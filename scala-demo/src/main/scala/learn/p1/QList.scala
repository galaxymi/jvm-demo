package learn.p1

sealed trait QList[+A]
case object Nil extends QList[Nothing]
case class Cons[+A](head: A, tail: QList[A]) extends QList[A]

object QList {
  def foldRight[A, B](as: QList[A], z: B)(f: (A, B) => B): B = as match {
    case Nil => z
    case Cons(x, xs) => f(x, foldRight(xs, z)(f))
  }

  def foldRightViaFoldLeft[A, B](as: QList[A], z: B)(f: (A, B) => B): B =
    foldLeft(as, (b: B) => b)((g, a) => b => g(f(a, b)))(z)

  def foldRightViaFoldLeft_[A, B](as: QList[A], z: B)(f: (A, B) => B): B =
    foldLeft(reverse(as), z)((b, a) => f(a, b))

  def foldLeft[A, B](as: QList[A], z: B)(f: (B, A) => B): B = as match {
    case Nil => z
    case Cons(x, xs) => foldLeft(xs, f(z, x))(f)
  }

  def foldLeftViaFoldRight[A, B](as: QList[A], z: B)(f: (B, A) => B): B =
    foldRight(as, (b: B) => b)((a, g) => b => g(f(b, a)))(z)

  def sum(ints: QList[Int]): Int = foldLeft(ints, 0)(_ + _)
  def product(ds: QList[Double]): Double = foldLeft(ds, 1.0)(_ * _)

  def apply[A](as: A*): QList[A] =
    if (as.isEmpty) Nil
    else Cons(as.head, apply(as.tail: _*))
  def tail[A](xs: QList[A]): QList[A] = xs match {
    case Nil => Nil
    case Cons(_, t) => t
  }
  def setHead[A](xs: QList[A], x: A): QList[A] = xs match {
    case Nil => Nil
    case Cons(_, t) => Cons(x, t)
  }
  def drop[A](l: QList[A], n: Int): QList[A] =
    if (n == 0) l
    else l match {
      case Nil => Nil
      case Cons(_, t) => drop(t, n-1)
    }

  def dropWhile[A](l: QList[A])(f: A => Boolean): QList[A] = l match {
    case Nil => Nil
    case Cons(h, t) => if (f(h)) dropWhile(t)(f) else Cons(h, dropWhile(t)(f))
  }

  def init[A](l: QList[A]): QList[A] = l match {
    case Nil => Nil
    case Cons(h, Nil) => Nil
    case Cons(h, t) => Cons(h, init(t))
  }

  def length[A](as: QList[A]): Int = foldLeftViaFoldRight(as, 0)((n, _) => n + 1)

  def reverse[A](as: QList[A]): QList[A] = foldLeft(as, Nil: QList[A])((acc, h) => Cons(h, acc))

  def append[A](xs: QList[A], x: A): QList[A] = append(xs, QList(x))
  def append[A](l: QList[A], r: QList[A]): QList[A] = foldRightViaFoldLeft(l, r)((x, xs) => Cons(x, xs))

  def concat[A](xs: QList[QList[A]]): QList[A] = foldRightViaFoldLeft(xs, Nil: QList[A])(append)

  def map[A, B](xs: QList[A])(f: A => B): QList[B] = foldRightViaFoldLeft(xs, Nil: QList[B])((h, acc) => Cons(f(h), acc))

  def filter[A](xs: QList[A])(f: A => Boolean): QList[A] = foldRightViaFoldLeft(xs, Nil: QList[A])((a, as) => if (f(a)) Cons(a, as) else as)

  def flatMap[A, B](xs: QList[A])(f: A => QList[B]): QList[B] = foldRightViaFoldLeft(xs, Nil: QList[B])((h, acc) => append(f(h), acc))

  def filterViaFlatMap[A](xs: QList[A])(f: A => Boolean): QList[A] = flatMap(xs)(x => if (f(x)) QList(x) else Nil)

  def zipWith[A, B, C](l: QList[A], r: QList[B])(f: (A, B) => C): QList[C] = (l, r) match {
    case (Nil, _) => Nil
    case (_, Nil) => Nil
    case (Cons(h1, t1), Cons(h2, t2)) => Cons(f(h1, h2), zipWith(t1, t2)(f))
  }
}