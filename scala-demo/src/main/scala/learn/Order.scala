package learn

class Order(val product: String, val price: BigDecimal, val amount: Integer){
  def total: BigDecimal = price * BigDecimal(amount)
  def receipt = s"You ordered $amount $product. Please pay $$$total."
}
