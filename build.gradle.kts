allprojects {
    repositories {
        jcenter()
        maven {
            setUrl("https://dl.bintray.com/kotlin/ktor")
        }
    }
}

subprojects {
    group = "dev.ianz"
    version = "1.0"
}
